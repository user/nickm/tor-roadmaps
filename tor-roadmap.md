Title: Tor development roadmap draft
Use numbered headers: true
HTML use syntax: true

* Table of contents
{:toc}

BEFORE YOU READ MORE
====================

This document is a draft.  I'm writing a big list of all the projects in
tor -- that is to say, the program "tor" -- that I can think of right
now that I might like to do over the next 5 years or so.

The list here isn't in any particular order; some items may be
implausible or unrealistic; I probably have made mistakes; not every
item here is necessarily a good idea; I have probably forgotten a bunch
of important items.

All priority estimates and time estimates are purely based on my
intuition. Please ask "do you still mean that?" before relying on any
particular estimate.

- Nick

Protocol-level security
=======================

Our initial designs in Tor were based around a number of assumptions
which --ten years ago and based on the knowledge we had at the time--
appeared to be plausible.  We've gotten a lot of experience in
protocol design since then, however.

This section lists lots of improvements in our protocols that I think we
ought to do.

Guard node improvements
-----------------------

Our current guard node system was designed at a time when nobody much
used guard nodes: they were proposed by Wright et al (XX find cite), but
I believe we were the first system to deploy them, as a way to thwart
long-term profiling attacks.  The research literature was silent as to
how to choose guards, how long to keep them, and how many to have at
once, so we picked defaults for Tor more or less arbitrarily.

Since we deployed guard nodes, however, more research (XX cite) has
found trouble points in our original choice of parameters.  We have an
open proposal (see proposal 236) to choose better parameters, but some
of the hard part will be migrating to them effectively.

These changes will require more than simply changing some constants in
the code: using fewer guards will make clients less tolerant of
unreliable or slow guards, and will require some tightening of
assignment of the guard flag. Using guards for longer will require
clients to choose nodes for circuits not only based on whether they are
a guard, but on how long they have been a guard.  Proposal 236 addresses
these points.

Priority: **highest**. Work: **3-6** developer-months.

Improved link cryptography
--------------------------

All versions of the Tor link protocol -- the one that clients use to talk to
relays, and relays use to talk to each other -- are built on top of TLS.  But
TLS is a horribly complex system.  In trying to solve so many problems (real
and imagined), TLS has apparently grown beyond the abilities of most
programming teams to implement correctly.

And in fact, most of what TLS does, Tor doesn't need.  We don't need
renegotiation, session resumption, session tickets, or SNI.  We don't need
arbitrary EC curve points, 0-RTT handshakes, or support for a dozen
deprecated legacy ciphers.  We certainly don't need a heatbeat extension.  We
don't need X.509, or integration with an external CA infrastructure.

We should seriously investigate smaller-scope, higher-confidence alternatives
to TLS.  This is a less enormous problem than it might seem; the portion of
the TLS protocol we'd need to duplicate is quite low.

If we do this properly, we might wind up with a link protocol of
general use to other projects.  The difficulty is mainly going to be
determined by how general we attempt to make any solution.

Priority: **medium**. Work: **6-12** developer-months.

Improved public identity keys for Tor nodes
-------------------------------------------

We need to improve our use of public key cryptography in Tor to
identify routers.  Right now, even though our forward secrecy and
circuit-extension authentication is done with 256-bit elliptic curve
groups, and our authorities sign directories with 2048-bit RSA keys,
routers are still identified with RSA1024 public keys.

Changing this to a better signing system -- Ed25519 is what I plan --
isn't just a trivial switch, since all the software that depends on
the old identities expects to see them, and everything in Tor that
refers to nodes internally expects to refer to them by a 20-byte SHA1
digest of the RSA1024 identity key.  There's a design proposal to
implement this change (proposal 220), but it's going to need another
round of revision, and a fair amount of work to accomplish.

Priority: **highest**; prerequisite for improved hidden service
protocols. Work: **3-6** developer-months.

Improved hidden service protocols
---------------------------------

In response to some high-quality hidden service attack papers (XX cite)
in 2013, I wrote a design for improved hidden service
infrastructure. (See proposal 224.)  This design includes an improved
directory and naming infrastructure to prevent a wide variety of
targeted censorship attacks by making it impossible to predetermine
which hidden-service directory nodes will host a given service's
descriptor at a given time, and which will make stop enumeration attacks
by making it impossible for any party who does not already know a hidden
service's name to learn it or connect to it through participation in the
protocol.

The revised protocol will also add support for ntor in hidden services to
improve their cryptographic strength, and make the hidden service protocol
more flexible, to allow cryptographic improvements in the future.  It
will also allow hidden service identity keys to be stored offline, to
increase resilience to server compromise.

(XX anything else?)

Priority: **highest**. Work: **6-12** developer-months.

Postquantum cryptography
------------------------

If it turns out that quantum computers are possible at scale, most
public key cryptography (and nearly all _deployed_ public key
cryptography) will turn out to be breakable.

Unfortunately, there aren't many great options for systems that remain
secure in a world with quantum computing.  Some have enormous keys,
some are ridiculously slow, and so forth.  Of the ones that remain,
their cryptographic properties appear to be less well-analyzed than the
public key systems we're using today.

So if we adopt a postquantum algorithm, we should do it for forward
secrecy purposes, as a defense against an adversary who's recording
stuff today and waiting for major engineering advances. We should add a
postquantum algorithm as an adjunct to an existing forward secrecy
mechanism, not a replacement, so that if the postquantum algorithm turns
out to be cryptographically weak, we remain no weaker than the ECDH
we're using for the rest of our forward-secrecy handshakes.

(Note that "postquantum cryptography" is not the same as "quantum
cryptography". The former means "cryptography that will still work
against an adversary with a quantum computer"; the latter means
"cryptography that uses interesting facts about physics in order to send
messages from point A to point B in an eavesdropper-resistant way."
We're not doing the second one.)

Priority: **medium**. Work: **2-4** developer-months.

Improved relay cryptography
---------------------------

Our relay cryptography is malleable, which amplifies some attacks (XX
cite) and requires some complicated workarounds (see proposal 209 for
refinements to one of these workarounds.  We've got a sketch of a design
for replacing our relay encryption entirely (see proposal 202), but
doing so requires us to have a high-quality, high-performance wide-block
cipher construction. There are some respected cryptographers working on
one; once they've got it done, we should build it into Tor.

Priority: **high**. Work: **2-4** developer-months.  Blocking on
results from cryptographers; we could probably get the cryptographers
more involved with a joint proposal.

Better DoS resistance
---------------------

To prevent denial of service attacks, it's a good idea to avoid points
in your protocol where servers respond to an easily generated query by
doing a complex operation. We have a few of these in Tor; it could be a
good idea to find ways to mitigate them.

Priority: **medium**. Work: **unknown** developer-months; will require
approx **1** developer-month to write a proposal and estimate task
scope.

Offline all long-term private keys
----------------------------------

When our systems use private keys to identity things -- relays,
authorities, and hidden services -- they ought to be designed so that
these private keys are stored offline.  Authority identity keys
already work this way: they are stored encrypted, used to sign
medium-term signing keys, and never loaded by the Tor process itself.
This kept us from having to replace these keys in the wake of the
Heartbleed bug; if we had a similar feature for other keys, then we
would have had to do no wide-scale key replacement.

We have designs moving relay identity keys offline as part of proposal
220, and doing the same for hidden services as part of proposal 224.

Priority: **high**. Work: included in work for proposals 220 and 224
noted above.


Program-level security
======================

We're engaged in a fundamentally difficult task: producing a secure C
program.  Given more resources and time, we can produce stronger results.

Privilege separation and division of responsibility
---------------------------------------------------

Right now, Tor runs as a single process: the process that uses the
network is the same as the part that parses directory documents,
implements the cryptography, provides hidden service logic, and chooses
how to build paths.  Because of this, all these parts run with the
same privileges.

We can do better.  (We already do, for authorities: authority identity
keys are stored offline and encrypted, and only handled by a separate
program.)  We could divide the Tor program into multiple pieces, and
have them all interact via IPC. By isolating responsibility in
different components, we can put isolate the most sensitive data
(private keys, user target addresses) in small, stable modules, while
restricting more volatile code from accessing sensitive data. Further,
we would have the freedom to develop non-performance-critical
components of Tor in higher level languages.

This architectural change would also improve the ability of
researchers to work on improvements to Tor, by making it easier to
replace components of the Tor server code in isolation.

If we do this right, along with the sandbox components discussed
below, it might be the basis of a general server-security toolkit that
other projects could use.

Priority: **medium**. Work: **6-24** developer months, depending on scope.

Improved sandboxing
-------------------

When running Tor 0.2.5 on Linux, we have code to support a
whole-process sandbox using the seccomp2 system call filter: by
blocking certain system calls and filtering others, we limit the
ability of an attacker who has compromised Tor to compromise other
pieces of the local host, install malware, or exfiltrate non-Tor data.

This is a good start, but there's room for improvement. First, it
would be great to support sandbox types on other platforms, notably
including OSX and Windows. We might be able to adapt code or
techniques from Google's chrome for this.  Second, it would be great
to restrict permissions even more tightly for separate pieces of Tor
itself. For example, we could restrict permission to access the
network to a small networking code of Tor, and prevent the other code
from touching the network at all.

Priority: **medium**. Work: **6-24** developer months, depending on scope.

Secure coding improvements
--------------------------

Tor is written in C; using the full feature set of C enables a
programmer to shoot themself in the foot with astounding power and
simplicity.  To avoid writing insecure code, we generally restrict
ourselves to a subset of allowable C styles and constructs.  We could
improve our choice of subset even further.

There are better practices we could adopt in order to lower the
probability of coding errors in our code.  We could use various
code-annotation-assisted analysis tools to try to get better checking
for our C code. Additionally, we could audit best practices from other
standards bodies and open source projects in order to find ones best
applicable to Tor, apply them to our software, and write up our
results for other projects to use.

It would also be great to replace as much of our hand-written C as
possible with less fragile code. We've covered elsewhere the idea of
replacing much of our C with higher level programming languages (XX
cite). Additionally, we could use machine-generated C to implement our
network protocol parsing code, which has historically been a sensitive
security point for network-facing C programs.  We'd have the option of
using an off-the-shelf data format (like ASN1, XDR, ProtocolBuffers,
etc) for newer protocols, but for our existing protocols, we'll need
to write our own tools from scratch.  This should be easier than it
sounds, since our formats are fairly simple and relatively uniform.

Priority: **high**. Work: **3-9** developer months, depending on scope.

Better resistance to memory- and cpu-based DoS
----------------------------------------------

In Tor 0.2.4.x and 0.2.5.x, in order to improve our resistance to the
so-called "sniper attack" (XX cite) wherein an attacker runs servers
out of memory in order to track users, we've worked to better track
our memory usage and kill circuits.  But there could be some
remaining subtler memory-based DoS attacks; and there are likely also
CPU-based DoS attacks.  We should spend developer time tracking
through our code's allocation patterns to see if we can find them, and
resolve them accordingly.

Priority: **medium**. Work: **3-6** developer months, depending on scope.

Better testing
--------------

This is a huge one for Tor's reliability in the future.  We've been
growing our test coverage slowly through the 0.2.5 release series.  On
the positive side, our test coverage for new code in 0.2.5 is higher
than in any previous Tor release: over 70% of new lines are covered by
our unit tests.  But on the negative side, test coverage for Tor as a
whole is still quite poor, at under 33%.

We need to improve this situation.  A good testing framework makes it
easier to develop software, by improving our ability to find mistakes
in our code before they get shipped to users.

One part of the solution is to include more unit tests as we develop
Tor.  By making Tor's components mockable, we've made unit tests
easier to write than previously: this accounts for much of the
improvement in our coverage.  We should, if possible, get the unit
test coverage for code in new releases even higher than in 0.2.5, and
in parallel write more unit tests to cover existing code.

The other part of the solution is better integration tests. We have a
simple testing framework, "chutney", that can launch a small Tor
network on a single development host and check whether it can deliver
traffic.  We should extend chutney to build more complex networks with
more diverse configuration, and to include scripted tests to test more
aspects of Tor's behavior.

Priority: **high**. Work: **3-12** developer months, depending on scope.

Security features XX
=================



Scalability and Performance
===========================

High-performance Tor servers
----------------------------

Current Tor servers overload one core of the CPUs, and barely use the
others. With some attention, we could improve Tor's scalability across
multiple cores to better saturate fat net connections.  We have
designs in this area, and some hope for initial work at distributing
some of our cryptographic functions across cores. It would be great to
expand that into full parallelization for our cryptography.

Fully parallelizing a Tor server is somewhat nontrivial compared to
many other networking servers, since on a typical Tor relay, every
network connection can potentially effect (nearly) every other network
connection, so it isn't possible to isolate related connections to
separate cores.  We'll need to investigate appropriate better data
structures for sharing data between cores if it turns out that
parallelizing the cryptography is not adequate in itself.

Once we've done this, we'll find other opportunities for performance
improvement.  Tor requires more cryptography than most networking
tools, so we can possibly benefit from facilities for off-loading
cryptography to GPUs or FPGAs.

Priority: **medium**. Work: **3-12** developer months depending on scope.

Support twenty thousand nodes
-----------------------------

Right now, we're sitting at around 4700 active nodes in the Tor
network. Of these, about 1200 provide 95% of the (weighted) bandwidth,
and about 2000 provide 99% of the bandwidth.  So let's call our
realistic network size 2000 nodes.

To continue to grow our capacity by 10 times, we'll need to tune our
node discovery algorithms somewhat. We can retain the assumption that
every client knows about every node, but we'll need to do some
engineering to lower the bandwidth required to synchronize this
information to clients.

Priority: **medium**. Work: **3-12** developer months depending on scope.

Support a one hundred thousand node network
-------------------------------------------

If we require a 50x increase in network size or more, that's the point
where I think our existing network topology assumptions will begin to
break down.  We'll no longer be able to assume that every user knows
about every node (directory communication overhead would grow
unreasonably high), or that every node can stay connected to every
other node (the per-connection overhead would run us out of file
descriptors if nothing else).  Instead, we'll need to look into new
topologies and plans for scaling.

Priority: **low**. Work: **unknown**; will take about **2-3**
developer-months, over the course of possibly a year to design a
solution and determine its scope.

Support large relay families
----------------------------

In Tor today, relays can declare themselves to belong to a single
"family" operated by a single or federated entity. This declaration
enables clients to route traffic so that no single family controls a
whole circuit.

Due to encoding issues, family declarations for routers consume
quadratic space, and are hard to maintain. But huge relay families may
be the future of a large tor network.  We should revise our family
declaration and advertising protocols to better support them.

Priority: **medium**. Work: **1-3** developer-months.

More performant scheduling and routing
--------------------------------------

There's been some research (XX cite, cite). XX

Reachability XX
============

IPv6 XX
----

Full DNS
--------

Tor currently supports only retrieving IPv4 and IPv6 records over
DNS. This prevents correct integration with DNSSEC, along with the
correct operation of many tools that need other DNS record types.

We have an open proposal (219) for implementing full DNS over Tor.

Full DNS over Tor would probably not be sufficient for usable DNSSEC
queries, though: regular DNSSEC requires multiple round trips, and
round trips over Tor are not performant.  We could, with significant
more expert and probably some time from a DNSSEC implementation
expert, design and implement protocol to collect all the records
needed to answer a DNSSEC query and send them back without multiple
round trips.

Priority: **medium**. Work: **3-5** developer-months for full DNS
support; an additional **1-2** Tor developer-months plus **3-8**
months from a DNSSEC domain expert for fast, usable DNSSEC support.

Pluggable-transport-related improvements XX
----------------------------------------

(Get George to write this.)

Client-to-relay XX
---------------

Relay-to-relay XX
--------------

Usability and security
======================

Application integration XX
-----------------------

Documentation
=============

Comprehensive technical user manual
-----------------------------------

The Tor program's current user-facing documentation is collected in a
Unix-style manual page documenting all of its options (XX cite).  But
this isn't really useful on its own: it doesn't really explain how tor
works, what it does, or why you might want to set most of the options
it provides.  We have documentation of this sort for new users (XX
cite), but for the more technically interested and sophisticated, it
might be good to have a better, more complete document explaining the
software that they're using.

Priority: **medium**. Work: **2** developer-months.

Developer documentation for Tor
-------------------------------

We have fairly good low-level documentation of our code: every
structure, every member, and nearly every function is documented now.
But the overall high-level structure and organization of the code is
not immediately apparent to a new developer, and is not well
documented anywhere.

A small first step would be to improve our documentation on getting
started with writing software for Tor; we could cover our coding
practices, our general code layout, and our testing strategy.

With more time, we could try to document our software more completely,
with high- and low-level overviews of all modules, their intended
interactions, and their actual behavior.

Priority: **high**. Work: **2** developer-months.

Revise technical specifications for completeness and clarity
------------------------------------------------------------

We're pretty proud of our specifications: they document what you need
to do to build a compatible Tor implementation, and they do it pretty
well.  But they have been composed in a patchwork fashion: whenever we
amend or extend the protocols, we revise the specifications.  The
effect of this history is that a once-clear set of documentations is
no longer straightforward reading for engineers who would like to
build compatible software.

To improve the documentation, we should refactor and revise our
technical specifications, with an eye to producing a modular and
slightly more formal overview of our protocol, with greater clarity on
which parts are and are not optional-to-implement.

Priority: **medium**. Work: **2** developer-months.

Research required XX
=================

Research on traffic analysis resistance TBD XX
-------------------------------------------


Research on path generation improvements XX
----------------------------------------

